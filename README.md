# Gitlab CI Runner Setup 

This project is used to setup a local gitlab runner on docker that runs gitlab ci jobs for a repository group.

> This is useful when the CI monthly minutes have expired.
>
> NOTE docker must be installed for this to work (e.g. docker for desktop)

### Registering the gitlab ci runner

From the root directory of this project:

- Create a new runner in the gitlab ui and copy the TOKEN.
- Create an secret .env file:
  ```
  CI_SERVER_TOKEN=%TOKEN GOES HERE%
  ```
- Then run
  ```
  docker-compose up register -d
  ```

This will create a mounted volume to a directory called `./runner_volume`.
This directory will contain the `config.toml` file generated after successful registration.

### Running the gitlab ci runner

After successully registering the runner from the root directory of this project

```
docker-compose up runner -d
```

Your hosted gitlab runner should now start process jobs.

### Resources

A very detailed article on hosting gitlab runners https://blog.hiebl.cc/posts/gitlab-runner-docker-in-docker/